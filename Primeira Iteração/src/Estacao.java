import java.util.LinkedList;
import java.util.List;

public class Estacao {

    private String zona;
    private String descricao;
    private int num_estacao;
    private List<Bilhete> v_entrada;
    private List<Bilhete> v_saída;

    public Estacao() {
        this.setV_entrada(new LinkedList<>());
        this.setV_saída(new LinkedList<>());
    }

    public Estacao(int num_estacao, String zona, String descricao ) {
        this.setV_entrada(new LinkedList<>());
        this.setV_saída(new LinkedList<>());
        this.setZona(zona);
        this.setDescricao(descricao);
        this.setNum_estacao(num_estacao);
    }

    /**
     * Adiciona o bilhete lista de entrada
     * @param b
     */
    public void adicionarValidacaoEntrada(Bilhete b) {
        this.getV_entrada().add(b);
    }

    /**
     * Adiciona o bilhete lista de saida
     * @param b
     */
    public void adicionarValidacaoSaida(Bilhete b) {
        this.getV_saída().add(b);
    }

    /**
     * Verifica se a lista de entrada e saida têm o mesmo tamanho
     * @return boolean
     */
    public boolean isValidacoesMesmoTamanho() {
        if (this.getV_entrada().size() == this.getV_saída().size()) {
            return true;
        }
        return false;
    }

    @Override
    public String toString(){
        return this.getNum_estacao() + " " + this.getDescricao() + " " + this.getZona();
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getNum_estacao() {
        return num_estacao;
    }

    public void setNum_estacao(int num_estacao) {
        this.num_estacao = num_estacao;
    }

    public List<Bilhete> getV_entrada() {
        return v_entrada;
    }

    public void setV_entrada(List<Bilhete> v_entrada) {
        this.v_entrada = v_entrada;
    }

    public List<Bilhete> getV_saída() {
        return v_saída;
    }

    public void setV_saída(List<Bilhete> v_saída) {
        this.v_saída = v_saída;
    }
}
