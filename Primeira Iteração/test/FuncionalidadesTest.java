import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FuncionalidadesTest {

    public FuncionalidadesTest() {

    }

    @Test
    public void numeroDePessoasEstacaoTest() {
        System.out.println("númeroDePessoasEstacao");
        Linha l = new Linha();
        Estacao e1 = new Estacao(1, "C1", "Hospital de S. João");
        Estacao e2 = new Estacao(2, "C1", "IPO");
        e1.adicionarValidacaoEntrada(new Bilhete("111992", "Z2", "1", "3"));
        e1.adicionarValidacaoEntrada(new Bilhete("111993", "Z2", "2", "5"));
        e1.adicionarValidacaoEntrada(new Bilhete("111994", "Z2", "2", "2"));
        e2.adicionarValidacaoEntrada(new Bilhete("111992", "Z2", "2", "4"));
        e2.adicionarValidacaoEntrada(new Bilhete("111993", "Z2", "4", "3"));
        l.getLinha().addLast(e1);
        l.getLinha().addLast(e2);
        ListIterator<Estacao> itr = l.getLinha().listIterator();
        ArrayList<Integer> num = Funcionalidades.numeroDePessoasEstacao(l, itr);
        int number1 = num.get(0);
        int number2 = num.get(1);
        assertTrue(number1 == 3 && number2 == 2);
    }

    @Test
    public void maiorEstacaoComTipoDeBilheteTest() {
        System.out.println("maiorEstacaoComTipoDeBilhete Com Z2");
        Linha l = new Linha();
        Estacao e1 = new Estacao(1, "C1", "Hospital de S. João");
        Estacao e2 = new Estacao(2, "C1", "IPO");
        l.getLinha().addLast(e1);
        l.getLinha().addLast(e2);
        Estacao result = Funcionalidades.maiorEstacaoComTipoDeBilhete(2, l.getLinha().listIterator());
        assertTrue(result.getNum_estacao() == 2);

    }

    @org.junit.Test
    public void transgressaoTest(){
        System.out.println("Transgressão");
        Linha l = new Linha();
        Estacao e1 = new Estacao(1, "C1", "Hospital de S. João");
        Estacao e2 = new Estacao(2, "C3", "IPO");
        e1.adicionarValidacaoEntrada(new Bilhete("111992", "Z2", "1", "2"));
        e1.adicionarValidacaoEntrada(new Bilhete("111992", "Z3", "1", "2"));
        l.getLinha().addLast(e1);
        l.getLinha().addLast(e2);
        List<Bilhete> lista = new ArrayList<>();
        lista.add(new Bilhete("111992", "Z2", "1", "2"));
        int size=lista.size();
        Assert.assertEquals(1,size);

    }
}
