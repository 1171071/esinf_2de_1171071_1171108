
import java.util.ArrayList;
import java.util.List;

/*
 * @author DEI-ESINF
 * @param <E>
 */
public class TREE<E extends Comparable<E>> extends BST<E> {

    /*
   * @param element A valid element within the tree
   * @return true if the element exists in tree false otherwise
     */
    public boolean contains(E element) {
        if (element == null) {
            return false;
        }
        Node<E> node = find(element, root);
        return node != null;
    }

    public boolean isLeaf(E element) {
        if (element == null) {
            return false;
        }
        Node<E> node = find(element, root);

        return (node == null ? false : node.getLeft() == null && node.getRight() == null);

    }

    /*
   * build a list with all elements of the tree. The elements in the 
   * left subtree in ascending order and the elements in the right subtree 
   * in descending order. 
   *
   * @return    returns a list with the elements of the left subtree 
   * in ascending order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes() {
        List<E> snapshot = new ArrayList<>();
        ascSubtree(root.getLeft(), snapshot);
        snapshot.add(root.getElement());
        desSubtree(root.getRight(), snapshot);
        return snapshot;
    }

    private void ascSubtree(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }
        ascSubtree(node.getLeft(), snapshot);
        snapshot.add(node.getElement());
        ascSubtree(node.getRight(), snapshot);
    }

    private void desSubtree(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }
        ascSubtree(node.getRight(), snapshot);
        snapshot.add(node.getElement());
        ascSubtree(node.getLeft(), snapshot);
    }

    /**
     * Returns the tree without leaves.
     *
     * @return tree without leaves
     */
    public BST<E> autumnTree() {
        TREE<E> autumn = new TREE();
        autumn.root = copyRec(root);
        return autumn;
    }

    private Node<E> copyRec(Node<E> node) {
        if (node == null) {
            return null;
        }
        if (node.getLeft() == null && node.getRight() == null) {
            return null;
        }
        return new Node<E>(node.getElement(), copyRec(node.getLeft()), copyRec(node.getRight()));
    }

    /**
     * @return the the number of nodes by level.
     */
    public int[] numNodesByLevel() {
        int dim = height(root) + 1;
        int[] nodeLevel = new int[dim];
        for (int i = 0; i < dim; i++) {
            nodeLevel[i] = 0;
        }
        numNodesByLevel(root, nodeLevel, 0);
        return nodeLevel;
    }

    private void numNodesByLevel(Node<E> node, int[] result, int level) {
        if (node == null) {
            return;
        }
        result[level] += 1;
        numNodesByLevel(node.getLeft(), result, level + 1);
        numNodesByLevel(node.getRight(), result, level + 1);
    }

    public E pathStrTree(String str) {
        return pathStrTree(root, str, 0);
    }

    private E pathStrTree(Node<E> node, String str, int pos) {
        E elem = null;
        if (pos < str.length() - 1 && node != null) {
            if (str.charAt(pos) == '0') {
                pathStrTree(node.getLeft(), str, pos + 1);
            }
            if (str.charAt(pos) == '1') {
                pathStrTree(node.getRight(), str, pos + 1);
            }
        }
        if (pos == str.length() - 1) {
            if (str.charAt(pos) == '0' && node.getLeft() != null) {
                elem = node.getLeft().getElement();
            }
            if (str.charAt(pos) == '1' && node.getRight() != null) {
                elem = node.getRight().getElement();
            }
        }
        return elem;
    }
}
