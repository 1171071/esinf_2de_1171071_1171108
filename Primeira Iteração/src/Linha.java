import java.util.ListIterator;

public class Linha {

    public DoublyLinkedList<Estacao> linha;

    public Linha() {
        this.setLinha(new DoublyLinkedList());
    }

    public static DoublyLinkedList<Estacao> sort(DoublyLinkedList<Estacao> list) {

        if (list.size() <= 1) {
            return list;
        }

        ListIterator<Estacao> itr = list.listIterator();

        DoublyLinkedList<Estacao> left = new DoublyLinkedList<Estacao>();
        DoublyLinkedList<Estacao> right = new DoublyLinkedList<Estacao>();
        DoublyLinkedList<Estacao> result = new DoublyLinkedList<Estacao>();

        int middle = list.size() / 2;

        for (int x = 0; x < middle; x++) {
            left.addLast(itr.next());
        }
        for (int x = middle; x < list.size(); x++) {
            right.addLast(itr.next());
        }

        left = sort(left);

        right = sort(right);

        result = merge(left, right);

        return result;
    }

    private static DoublyLinkedList<Estacao> merge(DoublyLinkedList<Estacao> left,
                                                   DoublyLinkedList<Estacao> right) {
        DoublyLinkedList<Estacao> result = new DoublyLinkedList<>();

        ListIterator<Estacao> itr1 = left.listIterator();
        ListIterator<Estacao> itr2 = right.listIterator();
        ListIterator<Estacao> itrf1 = left.listIterator();
        ListIterator<Estacao> itrf2 = right.listIterator();
        Estacao e1 = null, e2 = null, f1 = itrf1.next(), f2 = itrf2.next();
        boolean flag1 = false, flag2 = false;

        for (int y = 0; y < left.size() + right.size(); y++) {
            if (itr1.hasNext() && flag1 == false) {
                e1 = itr1.next();
            }
            if (itr2.hasNext() && flag2 == false) {
                e2 = itr2.next();
            }
            flag1 = true;
            flag2 = true;
            if (e1.getNum_estacao() < e2.getNum_estacao()) {
                result.addLast(e1);
                if (itr1.hasNext()) {
                    e1 = itr1.next();
                    if(f1.equals(e1)) {
                        e1 = itr1.previous();
                    }
                    flag1 = true;
                } else {
                    result.addLast(e2);
                }
            } else {
                result.addLast(e2);
                if (itr2.hasNext()) {
                    e2 = itr2.next();
                    if(f2.equals(e2)) {
                        e2 = itr1.previous();
                    }
                    flag2 = true;
                } else {
                    result.addLast(e1);
                }
            }
        }

        return result;
    }

    public DoublyLinkedList<Estacao> getLinha() {
        return linha;
    }

    public void setLinha(DoublyLinkedList<Estacao> linha) {
        this.linha = linha;
    }
}
