
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/*
 * This class contains all the 7 funcionalities requested in the assignement
 */
/**
 *
 * @author Bruno Pereira
 * @author Pedro Pinto
 */
public class Funcionalidades {

    private static Percurso percurso;

    /**
     * First Funcionality: Create Graph with the Metro Network
     */
    public static Graph<String, String> importData() throws IOException {
        Graph<String, String> rede = new Graph<>(true);                            // O(1)
        Map<String, Vertex<String, String>> linhaestacao = createStations();        // O(N)
        for (Vertex<String, String> v : linhaestacao.values()) {                    // O(N)
            rede.insertVertex(v.getElement());                                      // O(1)
        }
        for (String linha : linhaestacao.keySet()) {                                // O(N)
            rede = createConnections(linha, rede);                                  // O(N)
        }
        return rede;                                                                // O(1)
    }

    private static Map<String, Vertex<String, String>> createStations() throws IOException {
        String csvFile = "lines_and_stations.csv";                                  // O(1)
        BufferedReader br = null;                                                   // O(1)
        String line = "";                                                           // O(1)
        String cvsSplitBy = ";";                                                    // O(1)

        Map<String, Vertex<String, String>> estacoes = new LinkedHashMap<>();       // O(1)
        int countestacoes = 0;                                                      // O(1)

        try {
            br = new BufferedReader(new FileReader(csvFile));                       // O(1)
            while ((line = br.readLine()) != null) {                                // O(N)
                String[] temp = line.split(cvsSplitBy);                             // O(1)
                estacoes.put(temp[0], new Vertex<>(countestacoes, temp[1]));        // O(1)
                countestacoes++;                                                    // O(1)
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return estacoes;                                                            // O(1)  
    }

    public static LinkedList<Coordinates<String>> createCoordinates() throws IOException {
        String csvFile = "coordinates.csv";                                                                     // O(1)
        BufferedReader br = null;                                                                               // O(1)
        String line = "";                                                                                       // O(1)
        String cvsSplitBy = ";";                                                                                // O(1)

        LinkedList<Coordinates<String>> coordinates = new LinkedList<>();                                       // O(1)

        try {
            br = new BufferedReader(new FileReader(csvFile));                                                   // O(1)
            while ((line = br.readLine()) != null) {                                                            // O(N)
                String[] temp = line.split(cvsSplitBy);                                                         // O(1)
                Coordinates<String> c;                                                                          // O(1)
                c = new Coordinates<>(temp[0], Double.parseDouble(temp[1]), Double.parseDouble(temp[2]));       // O(1)
                coordinates.add(c);                                                                             // O(1)
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return coordinates;                                                                                     // O(1)

    }

    private static Graph<String, String> createConnections(String numline, Graph<String, String> g) throws IOException {
        String csvFile = "connections.csv";                                                                     // O(1)
        BufferedReader br = null;                                                                               // O(1)
        String line = "";                                                                                       // O(1)
        String cvsSplitBy = ";";                                                                                // O(1)

        try {
            br = new BufferedReader(new FileReader(csvFile));                                                   // O(1)
            while ((line = br.readLine()) != null) {                                                            // O(N)
                String[] temp = line.split(cvsSplitBy);                                                         // O(1)
                if (temp[0].equals(numline)) {                                                                  // O(1)
                    g.insertEdge(temp[1], temp[2], numline, Integer.parseInt(temp[3]));                         // O(1)             
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return g;                                                                                               // O(1)
    }

    /**
     * Second Funcionality: Verify if Graph is connected
     */
    public static LinkedList<Graph<String, String>> connections(Graph<String, String> rede) {
        LinkedList<Graph<String, String>> components = new LinkedList<>();                      //O(1)

        String[] all = rede.allkeyVerts();                                                      //O(N)
        if (isConnected(rede, all[0])) {                                                        //O(N)
            components.add(rede);                                                               //O(1)
            return components;                                                                  //O(1)

        } else {
            LinkedList<String> vertices = GraphAlgorithms.DepthFirstSearch(rede, all[0]);       //O(N^2)
            Graph<String, String> temp = new Graph(true);                                       //O(1)
            for (String v : vertices) {
                temp.insertVertex(v);                                                           //O(1)
            }
            boolean flag = false;                                                               //O(1)
            for (Graph g : components) {
                if (g.vertices().equals(temp.vertices())) {                                     //O(1)
                    flag = true;                                                                //O(1)
                }
            }
            if (!flag) {                                                                        //O(1)
                components.add(newGraph(temp, vertices));                                       //O(N)
            }

            for (String vert : rede.vertices()) {                                               //O(N)
                LinkedList<String> more = GraphAlgorithms.DepthFirstSearch(rede, vert);         //O(N^2)

                temp = new Graph(true);                                                         //O(1)
                for (String v : more) {
                    temp.insertVertex(v);                                                       //O(1)
                }

                flag = false;                                                                   //O(1)
                for (Graph g : components) {
                    if (g.vertices().equals(temp.vertices())) {                                 //O(1)
                        flag = true;                                                            //O(1)
                    }
                }

                if (more.size() != vertices.size() && !flag) {                                  //O(1)
                    components.add(temp);                                                       //O(1)
                }
                if (more.size() > vertices.size()) {                                            //O(1)    
                    vertices = more;                                                            //O(1)
                }
            }
        }
        return components;                                                                      //O(1)
    }

    public static boolean isConnected(Graph<String, String> g, String vert) {
        LinkedList<String> vertices = GraphAlgorithms.DepthFirstSearch(g, vert);                //O(N)
        if (vertices.size() == g.numVertices()) {                                               //O(1)
            return true;                                                                        //O(1)
        }
        return false;                                                                           //O(1)
    }

    public static Graph<String, String> newGraph(Graph<String, String> g, LinkedList<String> vertices) {
        Graph<String, String> clone = g.clone();                                                //O(1)
        for (String vert : vertices) {                                                          //O(N)
            clone.insertVertex(vert);                                                           //O(1)
        }
        return clone;                                                                           //O(1)
    }

    /**
     * Third Funcionallity : Creation of Percurso class
     */
    public static Percurso novoPercurso(Graph<String, String> rede, String station) {
        percurso = new Percurso(rede, station);                                                //O(1)
        return percurso;                                                                       //O(1)
    }

    /**
     * 4º Funcionallity
     *
     * @param g
     * @param vOrig
     * @param vDest
     * @param shortpath
     * @return
     */
    public static Percurso caminhoMinNumEstacoes(Graph<String, String> g, String vOrig, String vDest, LinkedList<String> shortpath) {
        shortpath.clear();                                                      //O(N)
        GraphAlgorithms.shortestPathEstacoes(g, vOrig, vDest, shortpath);       //O(N^2)
        shortpath = revPath(shortpath);                                         //O(N)
        Percurso p = new Percurso(g, shortpath);                                //O(N)

        return p;                                                               //O(1)
    }

    /**
     * 5º Funcionallity
     *
     * @param g
     * @param vOrig
     * @param vDest
     * @param shortpath
     * @return
     */
    public static Percurso caminhoMinTempo(Graph<String, String> g, String vOrig, String vDest, LinkedList<String> shortpath) {
        shortpath.clear();                                                      //O(N)
        GraphAlgorithms.shortestPath(g, vOrig, vDest, shortpath);               //O(N^2)
        shortpath = revPath(shortpath);                                         //O(N)
        Percurso p = new Percurso(g, shortpath);                                //O(N)

        return p;                                                               //O(1)
    }

    /**
     * Sixth Funcitionallity
     */
    public static Percurso caminhoMinLinhas(Graph<String, String> g, String vOrig, String vDest, LinkedList<String> shortpath) {
        shortpath.clear();                                                                  //O(N)
        Percurso p;                                                                         //O(1)
        ArrayList<LinkedList<String>> lista = GraphAlgorithms.allPaths(g, vOrig, vDest);    //O(N^2)
        p = new Percurso(g, lista.get(0));                                                  //O(N)
        percurso = p;                                                                       //O(1)
        for (LinkedList<String> sublist : lista) {                                          //O(N)
            Percurso temp = new Percurso(g, sublist);                                       //O(N)
            if (temp.getLines().size() <= p.getLines().size()) {                            //O(1)
                percurso = temp;                                                            //O(1)
            }
        }
        return percurso;                                                                    //O(1)
    }

    /**
     * Seventh Funcionality: Shortest path (regarding time) between vOrig and
     * vDest, passing through a list of intermidiate stations
     */
    public static Percurso shortestPathPassingThroughIntermidites(Graph<String, String> rede, String vOrig, String vDest, ArrayList<String> intermedias) {
        ArrayList<LinkedList<String>> caminhos = new ArrayList<>();             //O(1)
        percurso = new Percurso(rede, vOrig);                                   //O(1)
        LinkedList<String> t = new LinkedList<>();                              //O(1)
        LinkedList<LinkedList<String>> cam = new LinkedList<>();                //O(1)
        LinkedList<LinkedList<String>> possibilites = new LinkedList<>();       //O(1)

        int ind = 0, y = 0, tamanho = intermedias.size();                       //O(1)
        String temp = vOrig;                                                    //O(1)

        possibilites = generatePerm(intermedias);                               //O(N^2)

        while (y < possibilites.size()) {                                       //O(N) 
            t = new LinkedList<>();                                             //O(1)
            if (ind == tamanho) {                                               //O(1)
                GraphAlgorithms.shortestPath(rede, temp, vDest, t);             //O(N^2)
                cam.add(t);                                                     //O(1)
                caminhos.addAll(cam);                                           //O(1)
                cam = new LinkedList<>();                                       //O(1)
                temp = vOrig;                                                   //O(1)
                y++;                                                            //O(1)
                ind = 0;                                                        //O(1)
            } else {
                String second = possibilites.get(y).get(ind);                   //O(1)
                GraphAlgorithms.shortestPath(rede, temp, second, t);            //O(N^2)
                cam.add(t);                                                     //O(1)
                temp = second;                                                  //O(1)
                ind++;                                                          //O(1)
            }
        }

        LinkedList<LinkedList<String>> novosrumos = new LinkedList<>();         //O(1)
        LinkedList<String> dummy = new LinkedList<>();                          //O(1)
        int count = 0;                                                          //O(1)

        for (LinkedList<String> rumo : caminhos) {                              //O(N)
            rumo = revPath(rumo);                                               //O(N)
            if (count == 0) {                                                   //O(1)
                dummy.addAll(rumo);                                             //O(1)
                count++;                                                        //O(1)
            } else if (count == tamanho) {                                      //O(1)
                rumo.removeFirst();                                             //O(1)
                dummy.addAll(rumo);                                             //O(1)
                novosrumos.add(dummy);                                          //O(1)
                count = 0;                                                      //O(1)
                dummy = new LinkedList<>();                                     //O(1)
            } else {
                rumo.removeFirst();                                             //O(1)
                dummy.addAll(rumo);                                             //O(1)
                count++;                                                        //O(1)
            }
        }

        int s = Integer.MAX_VALUE;                                              //O(1)
        LinkedList<String> finallist = new LinkedList<>();                      //O(1)
        for (LinkedList<String> ca : novosrumos) {                              //O(N)
            if (ca.size() < s) {                                                //O(1)
                s = ca.size();                                                  //O(1)
                finallist = ca;                                                 //O(1)
            }
        }

        for (String station : finallist) {                                      //O(N)
            if (!station.equals(vOrig)) {                                       //O(1)
                percurso.addStation(station);                                   //O(1)
            }
        }

        return percurso;                                                        //O(1)
    }

    public static LinkedList<LinkedList<String>> generatePerm(ArrayList<String> original) {
        if (original.size() == 0) {                                             //O(1)
            LinkedList<LinkedList<String>> result = new LinkedList<>();         //O(1)
            result.add(new LinkedList<>());                                     //O(1)
            return result;                                                      //O(1)
        }
        String primeiro = original.remove(0);                                   //O(1)
        LinkedList<LinkedList<String>> result = new LinkedList<>();             //O(1)
        LinkedList<LinkedList<String>> permutations = generatePerm(original);   //O(1)
        for (List<String> perms : permutations) {                               //O(N)
            for (int index = 0; index <= perms.size(); index++) {               //O(N)
                LinkedList<String> temp = new LinkedList<>(perms);              //O(1)
                temp.add(index, primeiro);                                      //O(1)
                result.add(temp);                                               //O(1)
            }
        }
        return result;                                                          //O(1)
    }

    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);                        //O(1)
        LinkedList<V> pathrev = new LinkedList<>();                             //O(1)

        while (!pathcopy.isEmpty()) {                                           //O(N)
            pathrev.push(pathcopy.pop());                                       //O(1)
        }

        return pathrev;                                                         //O(1)
    }

}
