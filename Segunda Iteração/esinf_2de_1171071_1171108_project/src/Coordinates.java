/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bruno Pereira
 */
public class Coordinates<V> {
    
    private V estacao;
    private double latitude;
    private double longitude;
    
    public Coordinates(){
        this.estacao = null;
        this.latitude = 0;
        this.longitude = 0;
    }
    
    public Coordinates(V estacao, double latitude, double longitude) {
        this.estacao = estacao;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * @return the estacao
     */
    public V getEstacao() {
        return estacao;
    }

    /**
     * @param estacao the estacao to set
     */
    public void setEstacao(V estacao) {
        this.estacao = estacao;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    
}
