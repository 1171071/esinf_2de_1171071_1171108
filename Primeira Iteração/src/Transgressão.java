import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Transgressão {

    public static List<Bilhete> isInTransg(Linha l){
        List<Bilhete> lista = new ArrayList<>();
        List<Bilhete> listaBilhetesTotal = new ArrayList<>();
        /**
         * Juntar os bilhetes todos de todas as estações
         */
             for(Estacao e:l.linha){
                 for(Bilhete b:e.getV_entrada()){
                     listaBilhetesTotal.add(b);
                 }
             }
        /**
         * para cada Bilhete encontrar a sua estação origem e destino
         * @returns lista   lista com bilhetes em transgressão
         */
             for(Bilhete b : listaBilhetesTotal){
                 Estacao eOrigem= findEstacaoByBilhete(l, Integer.parseInt(b.getEstacaoOrigem()));
                 Estacao eDestino= findEstacaoByBilhete(l, Integer.parseInt(b.getEstacaoDestino()));

                 String [] temp= eOrigem.getZona().split("");
                 int cZonaO=Integer.parseInt(temp[1]);

                 temp= eDestino.getZona().split("");
                 int cZonaD=Integer.parseInt(temp[1]);

                 temp=b.getTipo().split("");
                 int zona=Integer.parseInt(temp[1]);

                 if(Math.abs(cZonaO-cZonaD+1)>=zona){
                     lista.add(b);
                 }
             }

        return lista;
    }

    /**
     *
     * @param l
     * @param num
     * @return Estacao
     */
    public static Estacao findEstacaoByBilhete(Linha l, int num){

        for(Estacao e: l.linha){
            if(e.getNum_estacao() == num){
                return e;
            }
        }


        return null;

    }


}
