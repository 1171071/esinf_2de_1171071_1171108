import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Funcionalidades {

    static Linha l = null;
    static ListIterator<Estacao> itr = null;
    static Scanner in = new Scanner(System.in);

    /**
     * Carrega dados a partir de um file.txt
     */
    public static void carregarDados() {
        l = new Linha();
        try {
            File f = new File("Estacoes");
            FileReader r = new FileReader(f);
            BufferedReader reader = new BufferedReader(r);

            String line;
            while ((line = reader.readLine()) != null) {
                String[] aux = line.split(",");
                int num = Integer.parseInt(aux[0]);
                Estacao e = new Estacao(num, aux[2], aux[1]);
                l.linha.addLast(e);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        itr = l.linha.listIterator();

        try {
            File f = new File("Viagens");
            FileReader r = new FileReader(f);
            BufferedReader reader = new BufferedReader(r);

            String line;
            while ((line = reader.readLine()) != null) {
                String[] aux = line.split(",");
                Bilhete b = new Bilhete(aux[0], aux[1], aux[2], aux[3]);
                int numEstacaoE = Integer.parseInt(aux[2]);
                Estacao e = procurarEstacao(l, numEstacaoE);
                e.adicionarValidacaoEntrada(b);

                int numEstacaoS = Integer.parseInt(aux[3]);
                Estacao s = procurarEstacao(l, numEstacaoS);
                e.adicionarValidacaoSaida(b);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Contabiliza o numero de pessoas por estação
     * @param l
     * @param itr
     * @return ArrayList<Integer>
     */
    public static ArrayList<Integer> numeroDePessoasEstacao(Linha l, ListIterator<Estacao> itr) {
        int cont = 1;
        Estacao e = null;
        ArrayList<Integer> num = new ArrayList<>();
        for (int i = 0; i < l.linha.size(); i++) {
            boolean flag = true;
            e = itr.next();
            List<Bilhete> temp = e.getV_entrada();
            num.add(contarPessoas(temp, flag, i, cont));
            System.out.println("Na estação " + e.getDescricao() + " passaram " + num.get(i) + " pessoas");
        }
        return num;
    }

    /**
     * Devolve a maior sequência de	estações que é possível	percorrer
     * @param count
     * @param itr
     * @return Estacao
     */
    public static Estacao maiorEstacaoComTipoDeBilhete(int count, ListIterator<Estacao> itr) {
        Estacao e = itr.next();
        String currentzona = e.getZona();
        int current = 0;
        Estacao returned = checkMaior(currentzona, current, count,itr);
        System.out.println(returned.toString());
        return returned;
    }

    /**
     * dando um numero devolve uma estacao
     * @param l
     * @param aux
     * @return Estacao
     */
    private static Estacao procurarEstacao(Linha l, int aux) {
        int i;
        ListIterator<Estacao> t = l.linha.listIterator();
        Estacao temp = t.next();
        while (aux != temp.getNum_estacao()) {
            temp = t.next();
        }

        return temp;
    }

    /**
     *
     * @param currentzona
     * @param current
     * @param count
     * @param itr
     * @return Estacao
     */
    private static Estacao checkMaior(String currentzona, int current, int count, ListIterator<Estacao> itr) {
        Estacao e = null;
        while (itr.hasNext()) {
            e = itr.next();
            String temp = e.getZona();
            if (!temp.equals(currentzona)) {
                current++;
            }
            if (current > count) {
                return itr.previous();
            }
        }
        return e;

    }

    /**
     * devolve o numero de pessoas numa estação
     * @param temp
     * @param flag
     * @param i
     * @param cont
     * @return int
     */
    private static int contarPessoas(List<Bilhete> temp, boolean flag, int i, int cont) {
        int j = 0;
        for (j = i + 1; j < temp.size(); j++) {
            flag = true;
            if (temp.get(i).getNumero() == temp.get(j).getNumero()) {
                flag = false;
            }
            if (flag) {
                cont++;
            }
        }
        if(j ==2){
            return cont+1;
        }
        return cont;
    }
}
