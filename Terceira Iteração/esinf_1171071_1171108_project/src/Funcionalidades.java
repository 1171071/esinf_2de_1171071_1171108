/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Bruno Pereira
 */
public class Funcionalidades extends BST<String> {

    static BST<MorseNode> morsetree, letterTree;

    static List<MorseNode> path1 = new ArrayList<>();
    static List<MorseNode> path2 = new ArrayList<>();

//Primeira Funcionalidade
    public static BST<MorseNode> createTree() {                                     //O(N)
        morsetree = new BST<>();                                                    //O(1)
        MorseNode root = new MorseNode("/", "Start", "Letter");                     //O(1)
        morsetree.insert(root);                                                     //O(1)
            
        String csvFile = "morse_v3.csv";                                            //O(1)
        BufferedReader br = null;                                                   //O(1)
        String line = "";                                                           //O(1)
        String cvsSplitBy = " ";                                                    //O(1)

        try {
            br = new BufferedReader(new FileReader(csvFile));                       //O(1)
            while ((line = br.readLine()) != null) {                                //O(N)
                String[] temp = line.split(cvsSplitBy);                             //O(1)                      
                MorseNode tempmorsenode = new MorseNode(temp[0], temp[1], temp[2]); //O(1)
                morsetree.insert(tempmorsenode);                                    //O(1)
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return morsetree;                                                       //O(1)
    }

    //Segunda Funcionalidade
    public static String morseToString(String morse) {                         
                                                       
        Node<MorseNode> node = morsetree.root;                                                          //O(1)                      
        String[] temp = morse.split(" ");                                                               //O(1)
        String convertida="";                                                                           //O(1)
        for(int i=0; i<temp.length;i++){                                                                //O(N)
          BST.Node<MorseNode> aux = morsetree.find(new MorseNode(temp[i].trim(), "", ""), node);        //O(1)
          if(temp == null){                                                                             //O(1)
              return "Morse Inválido";                                                                  //O(1)
          }
          convertida += aux.getElement().getSymbol();                                                   //O(1)
        }
        return convertida;                                                                              //O(1)

    }

    //Terceira Funcionalidade
    public static BST<MorseNode> createTreeLetters() {                                              //O(N)
        letterTree = new BST<>();                                                                     //O(1)
        Iterable<MorseNode> it = morsetree.inOrder();                                               
        Iterator itr = it.iterator();                                                               //O(1)
        while (itr.hasNext()) {                                                                     //O(N)
            Node<MorseNode> node = morsetree.find((MorseNode) itr.next(), morsetree.root);        //O(1)
            if (node.getElement().getCategory().equals("Letter")) {                                //O(1)
                letterTree.insert(node.getElement());                                               //O(1)
            }
        }
        return letterTree;                                                                          //O(1)
    }

    //Quarta Funcionalidade
    public static String letterToMorse(String word) {                                               //O(N^2)
        String result = "";                                                                         //O(1)  
        int count = 0;                                                                              //O(1)

        if (word.length() > 1) {                                                                    //O(1)
            while (count != word.length()) {                                                        //O(N)
                Iterable<MorseNode> it = letterTree.inOrder();
                Iterator nodes = it.iterator();                                                     //O(1)
                while (nodes.hasNext()) {                                                           //O(N)
                    MorseNode temp = (MorseNode) nodes.next();                                      //O(1)
                    if (temp.getSymbol().equals(String.valueOf(word.charAt(count)))) {              //O(1)
                        if (count == 0) {                                                           //O(1)
                            result = temp.getMorse();                                               //O(1)
                        } else {                                                                    //O(1)
                            result = result + " " + temp.getMorse();                                //O(1)
                        }
                        break;                                                                      //O(1)
                    }
                }
                if (result.equals("")) {                                                            //O(1)
                    return null;                                                                    //O(1)
                }
                count++;                                                                            //O(1)
            }
        } else {                                                                                    //O(1)
            Iterable<MorseNode> it = letterTree.inOrder();                          
            Iterator nodes = it.iterator();                                                         //O(1)
            while (nodes.hasNext()) {                                                               //O(N)
                MorseNode temp = (MorseNode) nodes.next();                                          //O(1)
                if (temp.getSymbol().equals(word)) {                                                //O(1)
                    result = temp.getMorse();                                                       //O(1)
                    break;                                                                          //O(1)
                }
            }
            if (result.equals("")) {                                                                //O(1)
                return null;                                                                        //O(1)
            }
        }
        return result;                                                                              //O(1)
    }
    
    //auxiliar para 5º funcionalidade
    public static String stringToMorse(String simbolo) {                                            //O(N^2)                                            
        
        String result = "";                                                                         //O(1)
                                                                              
            Iterable<MorseNode> it = morsetree.inOrder();                                           //O(1)
            Iterator nodes = it.iterator();                                                         //O(1)
            while (nodes.hasNext()) {                                                               //O(N)
                MorseNode temp = (MorseNode) nodes.next();                                          //O(1)
                if (temp.getSymbol().equals(simbolo)) {                                             //O(N)   
                    result = temp.getMorse();                                                       //O(1)
                    break;                                                                          //O(1)
                }
            }
            if (result.equals("")) {                                                                //O(1)
                return null;                                                                        
            }
        
        return result;                                                                              
    }

    // 5º Funcionalidade
    public static String sequenciaComum(String s1, String s2) {                                     //O(N^2)
        String result = null;                                                                       //O(1)
        Node<MorseNode> root = morsetree.root();                                                    //O(1)

        path1.clear();                                                                              //O(1)       
        path2.clear();                                                                              //O(1)

         //coverter o valor do node para morse 
        String resultAux = nodeComum(root, s1, s2);                                                 //O(N^2)
        result = stringToMorse(resultAux);                                                          //O(N^2)
        
         return result;

    }

    private static String nodeComum(Node<MorseNode> root, String s1, String s2) {                   //O(N^2)

        if (!morsetree.findPath(root, s1, path1) || !morsetree.findPath(root, s2, path2)) {         //O(N)
            return null;
        }

        int i;                                                                                      //O(1)
        for (i = 0; i < path1.size() && i < path2.size(); i++) {                                    //O(N)

            if (!path1.get(i).equals(path2.get(i))) {                                               //O(N)
                break;
            }
        }
        

        return path1.get(i - 1).getSymbol();                                                        //O(1)
    }
    
    
    //6º funcionalidade
    
    public static Map<String, Integer> ordenarListaPalavras(Map<String, Map<String, Integer>> mapa, String classe) //O(N^4)
    {
        
        List<MorseNode> classes = new ArrayList<>();                    //O(1)
        List<String> classesBST = new ArrayList<>();                    //O(1)
        classes = (List<MorseNode>) morsetree.inOrder();                //O(N)
        for (MorseNode nm : classes)                                    //O(N)
        {
            if (!classesBST.contains(nm.getCategory()) && !nm.getCategory().equalsIgnoreCase("root") && !nm.getCategory().equalsIgnoreCase("proSign"))//O(N)
            {
                classesBST.add(nm.getCategory());                       //O(1)
            }
        }

        for (String s : mapa.keySet())                                  //O(1)
        {

            for (String ss : classesBST)                                //O(N)
            {

                mapa.get(s).put(ss, 0);                                 //O(1)

            }
        }
        for (String s : mapa.keySet())                                  //O(N)
        {
            String temp[] = s.split(" ");                               //O(1)
            for (String ss : temp)                                      //O(N)
            {
                int cont = mapa.get(s).get(getClasse(ss));              //O(N^2)
                cont++;//o1
                mapa.get(s).put(getClasse(ss), cont);                   //O(1)
            }
        }
        
        Map<String, Integer> novoMap = new LinkedHashMap<>();           //O(1)
       for(String s : mapa.keySet()){                                   //O(N)
           novoMap.put(s, mapa.get(s).get(classe));                     //O(1)
           
       }
      Map<String, Integer> ordenarPerClasse = ordenarPerClasse(novoMap);//O(N)
        
        return ordenarPerClasse;                                        //O(1)
    }

    public static String getClasse(String temp)
    {

        for (MorseNode nm : morsetree.inOrder())                        //O(N)
        {
            if (nm.getMorse().equalsIgnoreCase(temp))                   //O(N)
            {
                return nm.getCategory();                                //O(1)
            }
        }
        return null;                                                    //O(1)
    }
    
    private static Map<String, Integer> ordenarPerClasse(Map<String, Integer> mapa){    //O(N)
        
        
        ValueComparator vl = new ValueComparator(mapa);                 //O(1)
        Map<String, Integer> sortedMapa = new TreeMap<>(vl);            //O(1)
        sortedMapa.putAll(mapa);                                        //O(N)
        
        return sortedMapa;                                              //O(1)
    }
}
