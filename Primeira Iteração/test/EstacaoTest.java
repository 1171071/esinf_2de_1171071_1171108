import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EstacaoTest {

    public EstacaoTest(){

    }

    @Test
    public void adicionarValidacaoEntradaTest(){
        System.out.println("adicionarValidacaoEntrada");
        Estacao e1 = new Estacao(1, "C1", "Hospital de S. João");
        e1.adicionarValidacaoEntrada(new Bilhete("111994", "Z2"));
        e1.adicionarValidacaoEntrada(new Bilhete("111995", "Z2"));
        assertTrue(e1.getV_entrada().size() == 2);
    }
    @Test
    public void adicionarValidacaoSaidaTest(){
        System.out.println("adicionarValidacaoSaida");
        Estacao e1 = new Estacao(1, "C1", "Hospital de S. João");
        e1.adicionarValidacaoSaida(new Bilhete("111994", "Z2"));
        e1.adicionarValidacaoSaida(new Bilhete("111995", "Z2"));
        assertTrue(e1.getV_saída().size() == 2);
    }
    @Test
    public void isValidacoesMesmoTamanhoTest() {
        System.out.println("isValidacoesMesmoTamanho");
        Estacao e1 = new Estacao(1, "C1", "Hospital de S. João");
        e1.adicionarValidacaoEntrada(new Bilhete("111996", "Z2"));
        e1.adicionarValidacaoSaida(new Bilhete("111994", "Z2"));
        assertTrue(e1.isValidacoesMesmoTamanho());
    }
}
