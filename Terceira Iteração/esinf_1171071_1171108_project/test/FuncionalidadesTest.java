
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Bruno Pereira
 */
public class FuncionalidadesTest {

    BST<MorseNode> morsetree, letterTree;

    public FuncionalidadesTest() {

    }

    @Before
    public void setUp() throws IOException {
        morsetree = Funcionalidades.createTree();
        letterTree = Funcionalidades.createTreeLetters();
    }

    @Test
    public void createTreeTest() {
        System.out.println("Create Tree");
        BST<MorseNode> instance = Funcionalidades.createTree();
        assertTrue("There should be 71 nodes including the Start", instance.size() == 71);
    }

    @Test
    public void morseToStringTest() throws IOException {
        setUp();
       
        
        System.out.println("Morse to String");
        String morse = "._ _... _._.";
        String expected = "ABC";
        String result = Funcionalidades.morseToString(morse);
        Assert.assertEquals(expected, result);
        morse = ". ... .. _. .._.";
        expected = "ESINF";
        result = Funcionalidades.morseToString(morse);
        Assert.assertEquals(". ... .. _. .._. is the ESINF", result, expected);
    }

    @Test
    public void createTreeLettersTest() throws IOException {
        setUp();
        System.out.println("Create Tree Letters");
        BST<MorseNode> instance = Funcionalidades.createTreeLetters();
        assertTrue("There should be 27 nodes including the Start", instance.size() == 27);
    }

    @Test
    public void letterToMorseTest() throws IOException {
        setUp();
        System.out.println("Letter to Morse");
        String expected = "_..";
        String word = "D";
        String result = Funcionalidades.letterToMorse(word);
        Assert.assertEquals("_.. is the D", result, expected);
        word = "ESINF";
        expected = ". ... .. _. .._.";
        result = Funcionalidades.letterToMorse(word);
        Assert.assertEquals(". ... .. _. .._. is the ESINF", result, expected);
        word = "123";
        expected = null;
        result = Funcionalidades.letterToMorse(word);
        Assert.assertEquals("null", result, expected);
    }

     @Test
    public void sequenciaComum() throws IOException {
        setUp();
        System.out.println("Morse to String");
        String simbo1 = "5";
        String simbo2 = "A";
        String expected = ".";
        String result = Funcionalidades.sequenciaComum(simbo1, simbo2);
        Assert.assertEquals( expected, result);
    }
    
    @Test
    public void testEx6() throws IOException
    {
        Map<String, Map<String, Integer>> mapa = new LinkedHashMap<>();
        mapa.put(".____ ..___ ...__", new LinkedHashMap<>());
        mapa.put("._ ..___ ...__", new LinkedHashMap<>());
        mapa.put("._ _... ...__", new LinkedHashMap<>());
        mapa.put("._ _... _._.", new LinkedHashMap<>());
        mapa.put("_._.__ ..___ _._.", new LinkedHashMap<>());
        mapa.put("_._.__ ..__.. _._.", new LinkedHashMap<>());
        mapa.put("_._.__ ..__.. _..._", new LinkedHashMap<>());
        System.out.println("ex6");
        setUp();
        String classe = "Letter";
            String[] expectedResult = new String[7];
            String[] result = new String[7];
            expectedResult[0] = "ABC";
            expectedResult[1] = "AB3";
            expectedResult[2] = "!?C";
            expectedResult[3] = "!2C";
            expectedResult[4] = "A23";
            expectedResult[5] = "!?=";
            expectedResult[6] = "123";
         int i=0;
        Map<String, Integer> ex6 = Funcionalidades.ordenarListaPalavras(mapa, classe);
        for(String s : ex6.keySet()){
            result[i] = Funcionalidades.morseToString(s);
            i++;
        }
        Assert.assertArrayEquals(expectedResult,result);
    }
}
