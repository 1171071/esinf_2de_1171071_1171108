
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Bruno Pereira
 */
public class FuncionalidadesTest {

    Graph<String, String> rede;
    LinkedList<Coordinates<String>> coordinates;
    LinkedList<String> shortpath;
    Percurso p;

    public FuncionalidadesTest() {
    }

    @Before
    public void setUp() throws IOException {
        rede = Funcionalidades.importData();
        coordinates = Funcionalidades.createCoordinates();
        shortpath = new LinkedList<>();
    }

    @Test
    public void importDataTest() throws IOException {
        System.out.println("Import Data");
        Graph<String, String> instance = Funcionalidades.importData();
        assertTrue("There should be 299 stations", instance.numVertices() == 299);
        assertTrue("There should be 700 connections", instance.numEdges() == 700);
    }

    @Test
    public void createCoordinatesTest() throws IOException {
        System.out.println("Create Coordinates");
        LinkedList<Coordinates<String>> instance = Funcionalidades.createCoordinates();
        assertTrue("There should be 299 stations", instance.size() == 299);
    }

    @Test
    public void connectionsTest() throws IOException {
        setUp();
        Graph<String, String> clone = rede.clone();
        System.out.println("Connections");

        LinkedList<Graph<String, String>> instance = Funcionalidades.connections(rede);
        assertTrue("Graph is connected so it only returns one element", instance.size() == 1);

        clone.removeEdge("Mairie d'Ivry", "Pierre Curie");
        clone.removeEdge("Pierre Curie", "Mairie d'Ivry");
        LinkedList<Graph<String, String>> instance2 = Funcionalidades.connections(clone);
        assertTrue("Returns 2 elements: the metro graph and the isolated station", instance2.size() == 2);

        clone.removeEdge("Mairie de Montreuil", "Croix de Chavaux");
        clone.removeEdge("Croix de Chavaux", "Mairie de Montreuil");
        LinkedList<Graph<String, String>> instance3 = Funcionalidades.connections(clone);
        assertTrue("Returns 2 elements: the metro graph and the two isolated stations", instance3.size() == 3);

        clone.removeEdge("Marcadet Poissonniers", "Simplon");
        clone.removeEdge("Simplon", "Marcadet Poissonniers");
        LinkedList<Graph<String, String>> instance4 = Funcionalidades.connections(clone);
        assertTrue("One element in list of components has 2 vertices", instance4.get(2).numVertices() == 2);
    }

    @Test
    public void createPercursoTest() throws IOException {
        setUp();
        System.out.println("Create Percurso");

        Percurso percurso = Funcionalidades.novoPercurso(rede, "Champs Elysees Clemenceau");
        assertTrue("The path starts at Champs Elysees Clemenceau and only has 1 station", percurso.getNumstations() == 1);
        percurso.addStation("Franklin Roosevelt");
        assertTrue("The path has 2 stations", percurso.getNumstations() == 2);
        percurso.addStation("Alma Marceau");
        assertTrue("The path passes through 2 lines", percurso.getLines().size() == 2);

        Iterator<String> it = percurso.stations().iterator();
        LinkedList<String> result = new LinkedList<>();

        while (it.hasNext()) {
            result.add(it.next());
        }
        assertTrue("Path is Champs Elysees Clemenceau, Franklin Roosevelt, Alma Marceau", result.equals(Arrays.asList("Champs Elysees Clemenceau", "Franklin Roosevelt", "Alma Marceau")));
    }

    @Test
    public void shortestPathPassingThroughIntermiditesTest() throws IOException {
        setUp();
        System.out.println("Shortest Path Passing Through Intermidites");
        String vOrig = "Villiers", vDest = "Cadet";
        ArrayList<String> intermedias = new ArrayList<>();
        intermedias.add("Saint Georges");
        intermedias.add("Temple");
        Percurso instance = Funcionalidades.shortestPathPassingThroughIntermidites(rede, vOrig, vDest, intermedias);
        assertTrue("The shortest path has 19 stations", instance.getNumstations() == 19);
        intermedias.add("Cadet");
        instance = Funcionalidades.shortestPathPassingThroughIntermidites(rede, vOrig, vDest, intermedias);
        assertTrue("Even if the final station is in the list, the method calculates the path and stops when that station is reached",
                instance.getNumstations() == 7);
    }

    @Test
    public void caminhoMinNumEstacoesTest() throws IOException {
        setUp();

        p = Funcionalidades.caminhoMinNumEstacoes(rede, "La Defense", "George V", shortpath);

        LinkedList<String> expectedResult = new LinkedList<>();
        expectedResult.add("La Defense");
        expectedResult.add("Esplanade de La Defense");
        expectedResult.add("Pont de Neuilly");
        expectedResult.add("Les Sablons");
        expectedResult.add("Porte Maillot");
        expectedResult.add("Argentine");
        expectedResult.add("Charles de Gaulle Etoile");
        expectedResult.add("George V");

        Iterator<String> it = p.stations().iterator();
        LinkedList<String> result = new LinkedList<>();

        while (it.hasNext()) {
            result.add(it.next());
        }

        assertTrue("Path should be La Defense, Esplanade de La Defense, Pont de Neuilly"
                + "Les Sablons, Porte Maillot, Argentine, Charles de Gaulle Etoile, George V", expectedResult.equals(result));
    }

    @Test
    public void caminhoMinTempoTest() throws IOException {
        setUp();

        p = Funcionalidades.caminhoMinTempo(rede, "La Defense", "Argentine", shortpath);

        LinkedList<String> expectedResult = new LinkedList<>();
        expectedResult.add("La Defense");
        expectedResult.add("Esplanade de La Defense");
        expectedResult.add("Pont de Neuilly");
        expectedResult.add("Les Sablons");
        expectedResult.add("Porte Maillot");
        expectedResult.add("Argentine");

        Iterator<String> it = p.stations().iterator();
        LinkedList<String> result = new LinkedList<>();

        while (it.hasNext()) {
            result.add(it.next());
        }

        assertEquals("Path should be La Defense, Esplanade de La Defense, Pont de Neuilly"
                + "Les Sablons, Porte Maillot, Argentine", expectedResult, result);
    }
    
     @Test
    public void caminhoMinLinhasTest() throws IOException {
        setUp();
        
        p = Funcionalidades.caminhoMinLinhas(rede, "Termes", "Abbesses", shortpath);
                
        assertTrue(p.getLines().size() == 2);
    }        
    
}
