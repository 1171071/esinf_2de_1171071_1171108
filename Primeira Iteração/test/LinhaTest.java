import org.junit.jupiter.api.Test;

import java.util.ListIterator;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LinhaTest {

    public LinhaTest() {
    }

    @Test
    public void testSort(){
        System.out.println("sort");
        DoublyLinkedList<Estacao> instance = new DoublyLinkedList<>();
        instance.addLast(new Estacao(9,null,null));
        instance.addLast(new Estacao(2,null,null));
        instance.addLast(new Estacao(5,null,null));
        instance.addLast(new Estacao(1,null,null));
        DoublyLinkedList<Estacao> result = Linha.sort(instance);
        ListIterator<Estacao> itr = result.listIterator();
        int n1 = itr.next().getNum_estacao();
        int n2 = itr.next().getNum_estacao();
        int n3 = itr.next().getNum_estacao();
        int n4 = itr.next().getNum_estacao();
        assertTrue(n1 < n2 && n2 < n3 && n3 < n4);

    }
}
