
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Bruno Pereira
 */
public class Percurso {

    private Graph<String, String> rede;
    private LinkedList<String> lines;
    private HashMap<Integer, String> stations;
    private int currenttime;
    private int numstations;
    private String currentline;

    public Percurso(Graph<String, String> rede, String station) {
        this.lines = new LinkedList<>();                                //O(1)
        this.stations = new HashMap<>();                                //O(1)
        this.currenttime = 0;                                           //O(1)
        this.currentline = "";                                          //O(1)
        this.stations.put(0, station);                                  //O(1)
        this.numstations = 1;                                           //O(1)
        this.rede = rede;
    }

    public Percurso(Graph<String, String> rede, LinkedList<String> list) {

        this.rede = rede;
        this.lines = new LinkedList<>();                                //O(1)
        this.stations = new HashMap<>();                                //O(1)
        this.currenttime = 0;                                           //O(1)
        this.currentline = "";                                          //O(1)
        this.stations.put(0, list.getFirst());                          //O(1)
        this.numstations = 1;                                           //O(1)
        for (String station : list) {                                   //O(N)
            if (!station.equals(stations.get(0))) {                     //O(1)
                addStation(station);                                    //O(1)
            }
        }
    }

    public void addStation(String station) {
        String first = stations.get(currenttime);                                //O(1)
        Edge<String, String> edge = getRede().getEdge(first, station);           //O(1)
        this.setCurrenttime((int) (this.getCurrenttime() + edge.getWeight()));   //O(1)
        this.setCurrentline(edge.getElement());                                  //O(1)
        this.stations.put(this.getCurrenttime(), station);                       //O(1)
        numstations++;                                                           //O(1)
        if (getLines().isEmpty()) {                                              //O(1)
            getLines().add(getCurrentline());                                    //O(1)

        } else {
            if (!lines.getLast().equals(currentline)) {                          //O(1)
                getLines().add(getCurrentline());                                //O(1)
            }
        }
    }

    public Iterable<String> stations() {
        return this.stations.values();
    }

    /**
     * @return the rede
     */
    public Graph<String, String> getRede() {
        return rede;
    }

    /**
     * @param rede the rede to set
     */
    public void setRede(Graph<String, String> rede) {
        this.rede = rede;
    }

    /**
     * @return the lines
     */
    public LinkedList<String> getLines() {
        return lines;
    }

    /**
     * @param lines the lines to set
     */
    public void setLines(LinkedList<String> lines) {
        this.lines = lines;
    }

    /**
     * @return the currenttime
     */
    public int getCurrenttime() {
        return currenttime;
    }

    /**
     * @param currenttime the currenttime to set
     */
    public void setCurrenttime(int currenttime) {
        this.currenttime = currenttime;
    }

    /**
     * @return the numstations
     */
    public int getNumstations() {
        return numstations;
    }

    /**
     * @param numstations the numstations to set
     */
    public void setNumstations(int numstations) {
        this.numstations = numstations;
    }

    /**
     * @return the currentline
     */
    public String getCurrentline() {
        return currentline;
    }

    /**
     * @param currentline the currentline to set
     */
    public void setCurrentline(String currentline) {
        this.currentline = currentline;
    }

    /**
     * @return the stations
     */
    public HashMap<Integer, String> getStations() {
        return stations;
    }

    /**
     * @param stations the stations to set
     */
    public void setStations(HashMap<Integer, String> stations) {
        this.stations = stations;
    }

}
