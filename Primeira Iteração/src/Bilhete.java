public class Bilhete {

    private String tipo;
    private String numero;
    private String estacaoOrigem;
    private String estacaoDestino;

    public Bilhete(String numero, String tipo, String estacaoOrigem,String estacaoDestino){
        this.setNumero(numero);
        this.setTipo(tipo);
        this.setEstacaoOrigem(estacaoOrigem);
        this.setEstacaoDestino(estacaoDestino);
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEstacaoOrigem() {
        return estacaoOrigem;
    }

    public void setEstacaoOrigem(String estacaoOrigem) {
        this.estacaoOrigem = estacaoOrigem;
    }

    public String getEstacaoDestino() {
        return estacaoDestino;
    }

    public void setEstacaoDestino(String estacaoDestino) {
        this.estacaoDestino = estacaoDestino;
    }
}
