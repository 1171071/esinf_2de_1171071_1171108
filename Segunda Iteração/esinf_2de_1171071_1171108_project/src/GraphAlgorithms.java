/*
* A collection of graph algorithms.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {
        boolean a = false;
        for (V vertex : g.vertices()) {
            if (vertex.equals(vert)) {
                a = true;
            }
        }
        if (!a) {
            return null;
        }
        boolean[] visited = new boolean[g.numVertices()];
        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        qbfs.add(vert);
        qaux.add(vert);
        visited[g.getKey(vert)] = true;
        while (!qaux.isEmpty()) {
            vert = qaux.pop();
            for (V vAdj : g.adjVertices(vert)) {
                if (visited[g.getKey(vAdj)] == false) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[g.getKey(vAdj)] = true;
                }
            }
        }
        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs) {
        qdfs.add(vOrig);                                        //O(1)
        visited[g.getKey(vOrig)] = true;                        //O(1)
        for (V vAdj : g.adjVertices(vOrig)) {                   //O(N)
            if (visited[g.getKey(vAdj)] == false) {             //O(1)
                DepthFirstSearch(g, vAdj, visited, qdfs);       //O(N)
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {
        boolean a = false;                                      //O(1)
        for (V vertex : g.vertices()) {                         //O(N)
            if (vertex.equals(vert)) {                          //O(1)
                a = true;                                       //O(1)
            }
        }
        if (!a) {                                               //O(1)
            return null;                                        //O(1)
        }
        LinkedList<V> qdfs = new LinkedList<>();                //O(1)
        boolean[] visited = new boolean[g.numVertices()];       //O(1)
        DepthFirstSearch(g, vert, visited, qdfs);               //O(N^2)
        return qdfs;                                            //O(1)
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest, boolean[] visited,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {
        visited[g.getKey(vOrig)] = true;                            //O(1)
        path.add(vOrig);                                            //O(1)
        Iterator<V> it = g.adjVertices(vOrig).iterator();           //O(1)  
        while (it.hasNext()) {                                      //O(N)
            V v = it.next();                                        //O(1)
            if (v.equals(vDest)) {                                  //O(1)
                path.add(vDest);                                    //O(1)
                LinkedList<V> reverse = revPath(path);              //O(N)
                paths.add(reverse);                                 //O(1)
                path.removeLast();                                  //O(1)
            } else {
                if (!visited[g.getKey(v)]) {                        //O(1)
                    allPaths(g, v, vDest, visited, path, paths);    //O(N)
                }
            }
        }
        V s = path.getLast();                                       //O(1)
        path.removeLast();                                          //O(1)
        visited[g.getKey(s)] = false;                               //O(1)
    }

    /**
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {
        boolean a = false;                                      //O(1)
        for (V vertex : g.vertices()) {                         //O(N)   
            if (vertex.equals(vOrig)) {                         //O(1)
                a = true;                                       //O(1)
            }
        }
        if (!a) {                                               //O(1)
            return null;                                        //O(1)
        }
        LinkedList<V> path = new LinkedList<>();                //O(1)
        ArrayList<LinkedList<V>> paths = new ArrayList<>();     //O(1)
        boolean[] visited = new boolean[g.numVertices()];       //O(1)
        allPaths(g, vOrig, vDest, visited, path, paths);        //O(N^2)
        return paths;                                           //O(1)
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param dist minimum distances
     */
    protected static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {
        int org = g.getKey(vOrig);                                                  //O(1)
        dist[org] = 0;                                                              //O(1)
        while (org != -1) {                                                         //O(N)
            visited[org] = true;                                                    //O(1)
            for (V vAdj : g.adjVertices(vOrig)) {                                   //O(N)
                int adj = g.getKey(vAdj);                                           //O(1)
                Edge<V, E> edge = g.getEdge(vOrig, vAdj);                           //O(1)
                if (!visited[adj] && dist[adj] > dist[org] + edge.getWeight()) {    //O(1)
                    dist[adj] = dist[org] + edge.getWeight();                       //O(1)
                    pathKeys[adj] = org;                                            //O(1)
                }
            }
            if (getVertMinDist(visited, dist) >= 0) {                               //O(1)
                vOrig = vertices[getVertMinDist(visited, dist)];                    //O(N)
                org = g.getKey(vOrig);                                              //O(1)
            } else {
                org = -1;                                                           //O(1)
            }
        }
    }

    private static <V> int getVertMinDist(boolean[] visited, double[] dist) {
        int vOrig = -1;
        double min = Double.MAX_VALUE;                                              //O(1)
        for (int i = 0; i < dist.length; i++) {                                     //O(N)                 
            if (visited[i] == false && dist[i] <= min) {                            //O(1)
                min = dist[i];                                                      //O(1)
                vOrig = i;                                                          //O(1)
            }
        }
        return vOrig;                                                               //O(1)
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    protected static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {
        path.add(vDest);                                                        //O(1)
        while (!vDest.equals(vOrig)) {                                          //O(N)
            int now = g.getKey(vDest);                                          //O(1)
            int next = pathKeys[g.getKey(vDest)];                               //O(1)
            if (next != -1) {                                                   //O(1)
                vDest = verts[next];                                            //O(1)
                path.add(vDest);                                                //O(1)
            } else {
                vDest = verts[now - 1];                                         //O(1)
            }
        }
    }
    //shortest-path between vOrig and vDest

    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {
        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {                   //O(1)
            return 0;                                                           //O(1)
        }
        int nverts = g.numVertices();                                           //O(1)
        boolean[] visited = new boolean[nverts];                                //O(1)
        int[] pathKeys = new int[nverts];                                       //O(1)
        double[] dist = new double[nverts];                                     //O(1)
        V[] vertices = g.allkeyVerts();                                         //O(N)

        for (int i = 0; i < nverts; i++) {                                      //O(N)
            dist[i] = Double.MAX_VALUE;                                         //O(1)
            pathKeys[i] = -1;                                                   //O(1)
        }

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);        //O(N^2)
        for (int i = 0; i < nverts; i++) {                                      //O(N)
            if (dist[i] == Double.MAX_VALUE) {                                  //O(1)
                dist[i] = 0;                                                    //O(1)
            }
        }
        getPath(g, vOrig, vDest, vertices, pathKeys, shortPath);                //O(N)
        shortPath = revPath(shortPath);                                         //O(N)
        return dist[g.getKey(vDest)];                                           //O(1)
    }

    //shortest-path between voInf and all other
    public static <V, E> boolean shortestPaths(Graph<V, E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList<Double> dists) {

        if (!g.validVertex(vOrig)) {
            return false;
        }

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];
        V[] vertices = g.allkeyVerts();

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);

        dists.clear();
        paths.clear();
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        for (int i = 0; i < nverts; i++) {
            LinkedList<V> shortPath = new LinkedList<>();
            if (dist[i] != Double.MAX_VALUE) {
                getPath(g, vOrig, vertices[i], vertices, pathKeys, shortPath);
                shortPath = revPath(shortPath);
            }
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);                        //O(1)
        LinkedList<V> pathrev = new LinkedList<>();                             //O(1)
        
        while (!pathcopy.isEmpty()) {                                           //O(N)
            pathrev.push(pathcopy.pop());                                       //O(1)
        }

        return pathrev;                                                         //O(1)
    }

    public static <V, E> double shortestPathEstacoes(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortpath) {

        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {                   //O(1)
            return 0;                                                           //O(1)
        }

        int nVerts = g.numVertices();                                           //O(1)
        boolean[] visited = new boolean[nVerts];                                //O(1)
        int[] pathKeys = new int[nVerts];                                       //O(1)
        double[] dist = new double[nVerts];                                     //O(1)
        V[] vertices = g.allkeyVerts();                                         //O(N)

        for (int i = 0; i < nVerts; i++) {                                      //O(N)
            dist[i] = Double.MAX_VALUE;                                         //O(1)
            pathKeys[i] = -1;                                                   //O(1)
        }

        shortestPathEdges(g, vOrig, vertices, visited, pathKeys, dist);
        for (int i = 0; i < nVerts; i++) {                                      //O(N)
            if (dist[i] == Double.MAX_VALUE) {                                  //O(1)
                dist[i] = 0;                                                    //O(1)
            }
        }
        getPath(g, vOrig, vDest, vertices, pathKeys, shortpath);                //O(N)

        return dist[g.getKey(vDest)];                                           //O(1)
    }

    protected static <V, E> void shortestPathEdges(Graph<V, E> g, V vOrig, V[] vertices, boolean[] visited, int[] pathKeys, double[] dist) {
        int vOrig_index = g.getKey(vOrig);                                      //O(1)
        LinkedList<V> queue_aux = new LinkedList<>();                           //O(1)

        dist = new double[g.numVertices()];                                     //O(1)
        for (V v : g.vertices()) {                                              //O(N)
            dist[g.getKey(v)] = Integer.MAX_VALUE;                              //O(1)
            pathKeys[g.getKey(v)] = -1;                                         //O(1)
        }
        queue_aux.add(vOrig);                                                   //O(1)
        dist[vOrig_index] = 0;                                                  //O(1)

        while (!queue_aux.isEmpty()) {                                          //O(N)
            vOrig = queue_aux.pop();                                            //O(1)
            for (V vAdj : g.adjVertices(vOrig)) {                               //O(N)
                if (dist[g.getKey(vAdj)] == Integer.MAX_VALUE) {                //O(1)
                    dist[g.getKey(vAdj)] = dist[g.getKey(vOrig)] + 1;           //O(1)
                    pathKeys[g.getKey(vAdj)] = g.getKey(vOrig);                 //O(1)
                    queue_aux.add(vAdj);                                        //O(1)
                }
            }
        }
    }
}
