
import java.util.Comparator;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class ValueComparator implements Comparator<String>{

    private Map<String, Integer> base;
    
    public ValueComparator(Map<String, Integer> base){
        this.base=base;
    }
    
    @Override
    public int compare(String t, String t1) {
        
        if(base.get(t) >= base.get(t1)){
            return -1;
        }else{
            return 1;
        }
    }
    
}
