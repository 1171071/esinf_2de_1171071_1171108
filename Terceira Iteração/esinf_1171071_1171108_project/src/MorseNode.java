/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bruno Pereira
 */
public class MorseNode implements Comparable<MorseNode> {

    private String morse, symbol, category;

    public MorseNode(String morse, String symbol, String category) {
        this.morse = morse;
        this.symbol = symbol;
        this.category = category;
    }

    public MorseNode(String morse) {
        this.morse = morse;
    }

    /**
     * @return the morse
     */
    public String getMorse() {
        return morse;
    }

    /**
     * @param morse the morse to set
     */
    public void setMorse(String morse) {
        this.morse = morse;
    }

    /**
     * @return the symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int compareTo(MorseNode o) {
        if (o.getMorse().length() > this.getMorse().length() && !o.getMorse().equals("/")) {
            for (int x = 0; x < this.getMorse().length(); x++) {
                if (this.morse.charAt(x) != o.getMorse().charAt(x)) {
                    if (o.getMorse().charAt(x) == '_') {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
            if (o.getMorse().charAt(getMorse().length()) == '_') {
                return -1;
            } else {
                return 1;
            }
        } else {
            return this.getMorse().compareTo(o.getMorse());
        }
    }

}
